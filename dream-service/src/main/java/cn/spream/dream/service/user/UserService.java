package cn.spream.dream.service.user;

import cn.spream.common.page.PaginatedList;
import cn.spream.dream.domain.user.User;
import cn.spream.dream.domain.user.UserQuery;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 13-6-6
 * Time: 上午12:04
 * To change this template use File | Settings | File Templates.
 */
public interface UserService {

    public boolean add(User user);

    public boolean deleteById(long id);

    public boolean updateById(User user);

    public User getById(long id);

    public PaginatedList<User> list(UserQuery userQuery);

}
