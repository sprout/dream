package cn.spream.dream.service.user.impl;

import cn.spream.common.page.PaginatedArrayList;
import cn.spream.common.page.PaginatedList;
import cn.spream.dream.dao.user.UserDao;
import cn.spream.dream.domain.user.User;
import cn.spream.dream.domain.user.UserQuery;
import cn.spream.dream.manager.user.UserManager;
import cn.spream.dream.service.BaseService;
import cn.spream.dream.service.user.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 用户业务逻辑处理层
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 13-6-6
 * Time: 上午12:09
 * To change this template use File | Settings | File Templates.
 */
@Service("userService")
public class UserServiceImpl extends BaseService implements UserService {

    private static final Log log = LogFactory.getLog(UserServiceImpl.class);

    @Resource(name = "userDao")
    private UserDao userDao;

    @Resource(name = "userManager")
    private UserManager userManager;

    /**
     * 添加用户信息
     *
     * @param user
     * @return
     */
    @Override
    public boolean add(User user) {
        boolean added = false;
        try {
            added = userManager.add(user);
        } catch (Exception e) {
            log.error("添加用户信息时出错了,user=" + user, e);
        }
        return added;
    }

    @Override
    public boolean deleteById(long id) {
        boolean deleted = false;
        try {
            if (id > 0) {
                deleted = userManager.deleteById(id);
            }
        } catch (Exception e) {
            log.error("删除用户信息时出错了,id=" + id, e);
        }
        return deleted;
    }

    @Override
    public boolean updateById(User user) {
        boolean updated = false;
        try {
            if (user != null && user.getId() > 0) {
                updated = userManager.updateById(user);
            }
        } catch (Exception e) {
            log.error("更新用户信息时出错了,user=" + user, e);
        }
        return updated;
    }

    @Override
    public User getById(long id) {
        User user = null;
        if (id > 0) {
            try {
                user = userManager.getUserWithCache(id);
            } catch (Exception e) {
                log.error("根据id获取用户信息时出错了,id=" + id, e);
            }
        }
        return user;
    }

    @Override
    public PaginatedList<User> list(UserQuery userQuery) {
        PaginatedList<User> profitsPaginated = new PaginatedArrayList<User>();
        try {
            int pageIndex = userQuery.getPageIndex();
            int pageSize = userQuery.getPageSize();
            profitsPaginated = new PaginatedArrayList<User>(pageIndex, pageSize);
            profitsPaginated.setTotalItem(userDao.count(userQuery));
            userQuery.setStartIndex(profitsPaginated.getStartIndex());
            userQuery.setPageSize(pageSize);
            profitsPaginated.addAll(userDao.list(userQuery));
        } catch (Exception e) {
            log.error("分页获取用户信息时出错了，userQuery=" + userQuery, e);
        }
        return profitsPaginated;
    }

}
