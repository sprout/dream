package cn.spream.dream.domain.user;

/**
 * 用户性别枚举类
 * Created by IntelliJ IDEA.
 * User: sjx
 * Date: 13-6-4
 * Time: 下午12:55
 * To change this template use File | Settings | File Templates.
 */
public enum SexEnum {

    MAN(0, "男"),
    WOMAN(1, "女");

    private int key;
    private String value;

    private SexEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public static SexEnum getEnumByKey(int key) {
        for (SexEnum sexEnum : SexEnum.values()) {
            if (sexEnum.getKey() == key) {
                return sexEnum;
            }
        }
        return null;
    }

    public static String getValueByKey(int key) {
        SexEnum sexEnum = getEnumByKey(key);
        return sexEnum == null ? null : sexEnum.getValue();
    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

}
