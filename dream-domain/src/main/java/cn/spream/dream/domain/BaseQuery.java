package cn.spream.dream.domain;

import cn.spream.common.page.PaginatedList;

/**
 * 基础查询类
 * Created by IntelliJ IDEA.
 * User: sjx
 * Date: 13-6-7
 * Time: 下午1:25
 * To change this template use File | Settings | File Templates.
 */
public class BaseQuery {

    protected int pageIndex = 1;
    protected int pageSize = 20;
    protected int startIndex;
    protected int startRow;
    protected int endRow;

    public BaseQuery() {
    }

    public BaseQuery(PaginatedList paginatedList) {
        this.pageIndex = paginatedList.getPageIndex();
        this.pageSize = paginatedList.getPageSize();
        this.startRow = paginatedList.getStartRow();
        this.endRow = paginatedList.getEndRow();
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getStartRow() {
        return startRow;
    }

    public void setStartRow(int startRow) {
        this.startRow = startRow;
    }

    public int getEndRow() {
        return endRow;
    }

    public void setEndRow(int endRow) {
        this.endRow = endRow;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }
}
