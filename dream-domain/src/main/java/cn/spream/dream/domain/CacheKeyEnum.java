package cn.spream.dream.domain;

/**
 * 缓存key枚举类
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 13-6-24
 * Time: 下午5:16
 * To change this template use File | Settings | File Templates.
 */
public enum CacheKeyEnum {

    USER("user_id_%d", 1000);

    private String key;
    private int exp;

    private CacheKeyEnum(String key, int exp) {
        this.key = key;
        this.exp = exp;
    }

    public String format(Object... values) {
        return String.format(key, values);
    }

    public int getExp() {
        return this.exp;
    }

}
