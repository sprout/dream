package cn.spream.dream.manager;

import cn.spream.common.cache.CacheHelper;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.Resource;

/**
 * 数据管理层
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 13-6-6
 * Time: 上午12:03
 * To change this template use File | Settings | File Templates.
 */
public class BaseManager {

    //编程式事务控制
    @Resource(name = "transactionTemplate")
    protected TransactionTemplate transactionTemplate;

    //缓存助手
    @Resource(name = "spyMemcachedHelper")
    protected CacheHelper cacheHelper;

}
