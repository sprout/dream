package cn.spream.dream.manager.user;

import cn.spream.dream.domain.user.User;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 13-6-5
 * Time: 下午11:28
 * To change this template use File | Settings | File Templates.
 */
public interface UserManager {

    public boolean add(User user);

    public boolean deleteById(long id);

    public boolean updateById(User user);

    public User getUserWithCache(long id);

    public boolean deleteUserWithCache(long id);

}
