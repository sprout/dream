package cn.spream.dream.dao.user.impl;

import cn.spream.dream.dao.BaseDao;
import cn.spream.dream.dao.user.UserDao;
import cn.spream.dream.domain.user.User;
import cn.spream.dream.domain.user.UserQuery;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户信息持久化
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 13-6-5
 * Time: 下午11:18
 * To change this template use File | Settings | File Templates.
 */
@Repository("userDao")
public class UserDaoImpl extends BaseDao implements UserDao {

    @Override
    public boolean add(User user) {
        return sqlSession.insert("user.add", user) == 1;
    }

    @Override
    public boolean deleteById(long id) {
        return sqlSession.delete("user.deleteById", id) == 1;
    }

    @Override
    public boolean updateById(User user) {
        return sqlSession.update("user.updateById", user) == 1;
    }

    @Override
    public User getById(long id) {
        return sqlSession.selectOne("user.getById", id);
    }

    @Override
    public User getByUsernameWithLogin(String username) {
        return sqlSession.selectOne("user.getByUsernameWithLogin", username);
    }

    @Override
    public int count(UserQuery userQuery) {
        return (Integer) sqlSession.selectOne("user.count", userQuery);
    }

    @Override
    public List<User> list(UserQuery userQuery) {
        return sqlSession.selectList("user.list", userQuery);
    }
}
