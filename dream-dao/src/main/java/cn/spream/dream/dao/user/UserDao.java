package cn.spream.dream.dao.user;

import cn.spream.dream.domain.user.User;
import cn.spream.dream.domain.user.UserQuery;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 13-6-5
 * Time: 下午11:15
 * To change this template use File | Settings | File Templates.
 */
public interface UserDao {

    public boolean add(User user);

    public boolean deleteById(long id);

    public boolean updateById(User user);

    public User getById(long id);

    public User getByUsernameWithLogin(String username);

    public int count(UserQuery userQuery);

    public List<User> list(UserQuery userQuery);

}
