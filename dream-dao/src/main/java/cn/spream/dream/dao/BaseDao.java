package cn.spream.dream.dao;

import org.apache.ibatis.session.SqlSession;

import javax.annotation.Resource;

/**
 * 数据持久层
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 13-6-5
 * Time: 下午10:58
 * To change this template use File | Settings | File Templates.
 */
public class BaseDao {

    @Resource(name = "sqlSession")
    protected SqlSession sqlSession;

}
