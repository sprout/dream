/**
 * JavaScript工具类
 * User: sjx
 * Date: 13-6-21
 * Time: 下午10:46
 * To change this template use File | Settings | File Templates.
 */
(function () {
    String.prototype.format = function (o) {
        return this.replace(/\{(\w+)\}/g, function (m, n) {
            return o[n];
        });
    };
})();