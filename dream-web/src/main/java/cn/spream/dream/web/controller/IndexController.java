package cn.spream.dream.web.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.util.SavedRequest;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 13-6-3
 * Time: 下午10:18
 * To change this template use File | Settings | File Templates.
 */
@Controller("indexController")
@RequestMapping("/")
public class IndexController extends BaseController {

    private final Log log = LogFactory.getLog(IndexController.class);

    /**
     * 首页，没有配置method则支持所有方式提交
     *
     * @param request
     * @param response
     * @param session
     * @param model
     * @return
     */
    @RequestMapping(value = "index")
    public String index(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
        return "index";
    }

    @RequestMapping(value = "404")
    public String notFound(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
        return "404";
    }

    @RequestMapping(value = "error")
    public String error(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
        return "error";
    }

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public String toLogin(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
        return "login";
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public String login(HttpServletRequest request, @RequestParam String username, @RequestParam String password, Model model) {
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(username, password);
        Subject subject = SecurityUtils.getSubject();
        try {
            subject.login(usernamePasswordToken);
        } catch (UnknownAccountException uae) {
            log.warn("登陆失败,未知账户.username=" + username);
        } catch (IncorrectCredentialsException ice) {
            log.warn("登陆失败,密码错误.username=" + username);
        } catch (LockedAccountException lae) {
            log.warn("登陆失败,账户锁定.username" + username);
        } catch (ExcessiveAttemptsException eae) {
            log.warn("登陆失败,错误次数过多.username" + username);
        } catch (AuthenticationException e) {
            log.error("登陆失败.username" + username, e);
        }
        if (subject.isAuthenticated()) {
            SavedRequest savedRequest = WebUtils.getSavedRequest(request);
            if (savedRequest != null) {
                return redirect(savedRequest.getRequestUrl());
            } else {
                return "index";
            }
        } else {
            usernamePasswordToken.clear();
            model.addAttribute("error", "用户与密码不一致");
            return "login";
        }
    }

}
