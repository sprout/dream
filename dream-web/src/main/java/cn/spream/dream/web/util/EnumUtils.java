package cn.spream.dream.web.util;

import cn.spream.dream.domain.user.SexEnum;

/**
 * 枚举工具类velocity视图使用
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 13-6-19
 * Time: 下午1:08
 * To change this template use File | Settings | File Templates.
 */
public class EnumUtils {

    public static String getUserSexValue(int key) {
        return SexEnum.getValueByKey(key);
    }

}
