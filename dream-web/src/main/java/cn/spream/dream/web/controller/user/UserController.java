package cn.spream.dream.web.controller.user;

import cn.spream.common.page.PaginatedList;
import cn.spream.dream.domain.user.User;
import cn.spream.dream.domain.user.UserQuery;
import cn.spream.dream.service.user.UserService;
import cn.spream.dream.web.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * 用户信息处理
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 13-6-6
 * Time: 上午12:14
 * To change this template use File | Settings | File Templates.
 */
@Controller("userController")
@RequestMapping("/user")
public class UserController extends BaseController {

    @Resource(name = "userService")
    private UserService userService;

    /**
     * 添加用户，只支持post提交
     *
     * @param user
     * @param totalPage
     * @return
     */
    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String add(User user, int totalPage, Model model) {
        boolean added = userService.add(user);
        model.addAttribute("pageIndex", String.valueOf(totalPage + 1));
        model.addAttribute("added", String.valueOf(added));
        return redirect("/user/list");
    }

    /**
     * 更新用户，只支持post提交
     *
     * @param user
     * @param pageIndex
     * @return
     */
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public String update(User user, int pageIndex, Model model) {
        boolean updated = userService.updateById(user);
        model.addAttribute("pageIndex", String.valueOf(pageIndex));
        model.addAttribute("updated", String.valueOf(updated));
        return redirect("/user/update/" + user.getId());
    }

    /**
     * 根据id删除用户信息，返回JSON类型结果
     * 1.produces:请求时Headers中Accept参数的值包含produces指定的类型时才允许访问此方法;
     * 2.Headers中Accept的含义:用来指定什么媒体类型的响应是可接受的，
     * 即告诉服务器我需要什么媒体类型的数据，此时服务器应该根据Accept请求头生产指定媒体类型的数据;
     * 3.@ResponseBody标示将返回结果直接写入HTTP response body中
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "delete/{id}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public boolean delete(@PathVariable long id) {
        boolean deleted = userService.deleteById(id);
        return deleted;
    }

    /**
     * 分页获取用户信息，只支持get请求
     *
     * @param userQuery
     * @param model
     * @return
     */
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String list(UserQuery userQuery, Model model) {
        PaginatedList<User> users = userService.list(userQuery);
        model.addAttribute("users", users);
        return "user/list";
    }

    /**
     * 跳转到添加用户页面
     *
     * @return
     */
    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String toAdd() {
        return "user/add";
    }

    /**
     * 跳转到更新用户页面
     *
     * @param id    通过请求地址获取用户id
     * @param model
     * @return
     */
    @RequestMapping(value = "update/{id}", method = RequestMethod.GET)
    public String toUpdate(@PathVariable long id, Model model) {
        User user = userService.getById(id);
        model.addAttribute("user", user);
        return "user/update";
    }

    /**
     * 根据id获取用户json数据
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "get/{id}/json", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public User getJSONById(@PathVariable long id) {
        User user = userService.getById(id);
        return user;
    }

}
