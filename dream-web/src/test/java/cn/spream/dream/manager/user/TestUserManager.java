package cn.spream.dream.manager.user;

import cn.spream.dream.BaseTest;
import cn.spream.dream.domain.user.SexEnum;
import cn.spream.dream.domain.user.User;
import org.junit.Assert;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 13-6-24
 * Time: 上午9:14
 * To change this template use File | Settings | File Templates.
 */
public class TestUserManager extends BaseTest {

    @Resource(name = "userManager")
    private UserManager userManager;

    @Test
    public void testAdd() {
        User user = new User();
        user.setName("沐丝");
        user.setSex(SexEnum.MAN.getKey());
        user.setAge(22);
        user.setBirthday(new Date());
        user.setPhone("88888888");
        user.setAddress("中国北京");
        user.setEmail("zhangsan@spream.cn");
        boolean added = userManager.add(user);
        Assert.assertTrue(added);
    }

}
