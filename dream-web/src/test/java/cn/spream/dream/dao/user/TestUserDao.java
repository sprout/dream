package cn.spream.dream.dao.user;

import cn.spream.dream.BaseTest;
import cn.spream.dream.domain.user.SexEnum;
import cn.spream.dream.domain.user.User;
import org.junit.Assert;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 13-6-18
 * Time: 上午9:01
 * To change this template use File | Settings | File Templates.
 */
public class TestUserDao extends BaseTest {

    @Resource(name = "userDao")
    private UserDao userDao;

    @Test
    public void testAdd() {
        User user = new User();
        user.setName("沐丝");
        user.setSex(SexEnum.MAN.getKey());
        user.setAge(22);
        user.setBirthday(new Date());
        user.setPhone("88888888");
        user.setAddress("中国北京");
        user.setEmail("zhangsan@spream.cn");
        boolean added = userDao.add(user);
        Assert.assertTrue(added);
    }

    @Test
    public void testAdds() {
        for (int i = 0; i < 102; i++) {
            testAdd();
        }
    }

}
